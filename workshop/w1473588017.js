// For the latest Axiom VirtualNetwork+ scripting documentation, 
// please visit: http://www.zachtronics.com/virtualnetwork/

function getTitle() {
    return "Revenge";
}

function getSubtitle() {
    return "Pool on the roof must have a leak";
}

function getDescription() {
    return "Find when the target (contained in file 300) has Advanced English (string in file 301).\n Set up a sprinkler test for that time.";
}

function initializeTestRun(testRun) {
	var names = ["Kate Libby","Dade Murphy","Emmanuel Goldstein","Paul Cook","Ramon Sanchez","Joey Pardella"];
    var passcode = randomChoice(["LOVE","SECRET","SEX","GOD","PASSWORD","GUEST","BATTERYHORSESTAPLECORRECT"]);
    var target = randomChoice(names);
	var indexfile=[];
	var hours = ["800","1000","1300"];
    var hour = randomChoice(hours);
    var opcode = randomChoice(["0X45","0X32","0XFE","0X01"]);
	var passwordfile = convertTextToKeywords("Jerks, I entered boop as my password, and the system still wouldn't open my bank ...");
	var passwordscold = convertTextToKeywords("Sir, we're tring to help you, so please remain calm. Also, as a reminder, never reveal your password to anyone ...");
	var phishingfile = convertTextToKeywords("Congratulations, for your history of excellence, you have been chosen to test our new software, attached below ...");
	var phishingoutfile = convertTextToKeywords("Hey, Jerks, I tried installing your software but all that happens is a window popped up and diappeared ...");
	var boatfile = convertTextToKeywords("Are you interested in being on the mailing list for newest boat listings? Please sign into your e-mail below ...");
	var manualfile = convertTextToKeywords("Thank you for trying AutoMate for Windows 3.11. Please note that this is a trial version and ...");
	var opcodefile = convertTextToKeywords("Capabilities vary by hardware: Lighing Controls 0x37 bool Alarm 0xAC bool Fire Suppression 0x00 time ...");
	var licensefile = convertTextToKeywords("This is not free software! If you continue to use AutoMate after 30 days, you must register it by mailing $95.00 to ...");
	var crackedfile = convertTextToKeywords("Cracked by DarkWraith69! Underlands BBS 47-2-6838-9876 2400, 9600, 14.4k, 28.8k");
	var techsupfile=convertTextToKeywords("Sir, as a reminder, you can't just set the data and expect a test to run, you need to tell the machine to run the test with a boolean value (1 or 0). This is for safety purposes, and no matter how often you request it, it will not be removed. ...");
    var inboxHost = createHost("Inbox", 5, 0, 3, 3);
    var secureHost = createHost("Secure", 10, 0, 3, 3);
    var outboxHost = createHost("Outbox", 5, 5, 3, 3);;
    var registrarHost = createHost("Registrar", 10, 5, 3, 3);
    var automationHost = createHost("Automation", 10, -5, 3, 3);
    createLink(inboxHost, 800, outboxHost, -1);
    createLink(inboxHost, 888, secureHost, -1);
    createLink(secureHost, 801, automationHost, -1);
    createLink(secureHost, 800, registrarHost, -1);
    var testReg = createRegister(automationHost, 12,-5, "TEST");
    var passcodeReg = createRegister(automationHost, 12,-4, "PASS");
    var arg1Reg = createRegister(automationHost, 10,-5, "ARG1");
    var arg2Reg = createRegister(automationHost, 10,-4, "ARG2");
	opcodefile[13]=opcode;
	passwordfile[4]=passcode;



    var goalTest = requireCustomGoal("Set a test to occur.");
    var goalCode = requireCustomGoal("Bypass Security Lockout.");
    var goalArg1 = requireCustomGoal("Set the time of the test.");
    var goalArg2 = requireCustomGoal("Set the type of the test.");
    mergeRequirements( 2, "Set the test arguments.");

 
    setRegisterWriteCallback(passcodeReg, function(value) {if (value == passcode){setCustomGoalCompleted(goalCode);}});
   setRegisterWriteCallback(testReg, function(value) {if (value == 1){setCustomGoalCompleted(goalTest);}});
   setRegisterWriteCallback(arg1Reg, function(value) {	   if (value == opcode){setCustomGoalCompleted(goalArg1);}});
   setRegisterWriteCallback(arg2Reg, function(value) {	   if (value == hour){setCustomGoalCompleted(goalArg2);}});
	 	createNormalFile(getPlayerHost(), 300, FILE_ICON_TEXT, [target]);
			 	createNormalFile(getPlayerHost(), 301, FILE_ICON_TEXT, ["Advanced English"]);
   


   temp=createNormalFile(inboxHost, 200, FILE_ICON_TEXT, passwordscold);
	 setFileInitiallyCollapsed(temp);
	    temp=createNormalFile(inboxHost, 201, FILE_ICON_TEXT, phishingfile);
	 setFileInitiallyCollapsed(temp);
	    temp=createNormalFile(inboxHost, 203, FILE_ICON_TEXT, boatfile);
	 setFileInitiallyCollapsed(temp);
	    temp=createNormalFile(outboxHost, 204, FILE_ICON_TEXT, passwordfile);
	 setFileInitiallyCollapsed(temp);
	 	    temp=createNormalFile(outboxHost, 205, FILE_ICON_TEXT, phishingoutfile);
	 setFileInitiallyCollapsed(temp);
	 	 	    temp=createNormalFile(inboxHost, 206, FILE_ICON_TEXT, techsupfile);
	 setFileInitiallyCollapsed(temp);
	 for (var i = 0; i < 6; i++) {
	var AI="";
	
	if(names[i]==target){AI=hour;} else {AI=String(100*i+800);}
	indexfile.push(250+i);

 temp=createNormalFile(registrarHost, 250+i , FILE_ICON_USER, [names[i],"Advanced English",AI,"Pre-Calculus",String(-100*i+1500),"Fundamentals of Computer Science",String(100*i+1200)]);
 setFileInitiallyCollapsed(temp);
 }
	 	temp=createLockedFile(secureHost, 270, FILE_ICON_DATA, manualfile);
	 setFileInitiallyCollapsed(temp);
	 	temp=createLockedFile(secureHost, 271, FILE_ICON_DATA, opcodefile);
	 setFileInitiallyCollapsed(temp);
	 	temp=createLockedFile(secureHost, 272, FILE_ICON_DATA, licensefile);
	 setFileInitiallyCollapsed(temp);
	 	temp=createLockedFile(secureHost, 280, FILE_ICON_DATA, crackedfile);
	 setFileInitiallyCollapsed(temp);
	 	temp=createLockedFile(registrarHost, 199, FILE_ICON_DATA, indexfile);
			 setFileInitiallyCollapsed(temp);


}
function onCycleFinished() {
}
